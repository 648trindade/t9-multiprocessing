import sys,os,time
from multiprocessing import Process, Queue

__author__ = 'rafael'

def wtime():
    return time.time().real

def read_in():
    dic = {}
    with open('dna.in') as dna:
        ls = dna.readlines()
        nome = ls[0]
        dic[nome] = ""
        for l in ls[1:]:
            if l[0] != '>':
                dic[nome] += l[:-1]
            else:
                nome = l
                dic[nome] = ""
    return dic

def gotoline(file, line):
    for i in range(line*2):
        file.readline()

def busca (query, dna):
    ret = []
    # organiza partes do genoma
    parts = list(dna.keys())
    parts.sort()
    parts = [parts[0]]+parts[2:]+[parts[1]]
    # parte a parte
    for nome in parts:
        # busca
        res = dna[nome].find(query)
        # se encontrou
        if res != -1:
            pass
            ret.append([nome,res])
    return ret

def query_read(dna,bounds,queue):
    with open('query.in') as query_file:
        gotoline(query_file,bounds[0])
        result = ""
        for i in range(bounds[1]):
            nome = query_file.readline()
            # testa se arquivo terminou
            if len(nome) == 0:
                break
            elif nome[0] == '>':
                # escreve nome da busca na saida
                result += nome
                # le linha a ser buscada e remove \n
                search = query_file.readline()[:-1]
                # recebe resultado da busca
                res = busca(search,dna)
                # se busca retornou algum resultado
                if len(res) > 0:
                    # itera resultados da busca
                    for part,pos in res:
                        # escreve resultados
                        result += part + str(pos)+'\n'
                else:
                    result += 'NOT FOUND\n'
    queue.put(result)
    return

def main():
    dna = read_in()
    query = open('query.in')
    out = open('dna.out','w')

    # calcula e distribui total de strings de busca
    p_num = int(sys.argv[1])
    total = len(query.readlines())//2
    query.close()
    num = [total//p_num for i in range(p_num)]
    for i in range(total%p_num):
        num[i] += 1
    div,acum = [[0,num[0]]],0
    for i in range(len(num)-1):
        acum += num[i]
        div.append([acum,num[i+1]])

    # cria fila e processos
    q = Queue(p_num)
    pro = [Process(target=query_read,args=(dna,div[i],q,)) for i in range(p_num)]
    outs = []

    start_time = wtime()
    # inicia processos
    for p in pro:
        p.start()
    # 'espia' a cada meio segundo pra ver se a fila de resultados nao esta cheia
    while not q.full():
        time.sleep(0.5)
    # junta os processos
    for p in pro:
        p.join(timeout=0.1)
    # retir e organiza dados da fila
    while q.qsize()>0:
        outs.append(q.get())
    outs.sort()
    # escreve resultados
    for o in outs:
        out.write(o)
    end_time = wtime()

    out.close()
    print (end_time-start_time)
    sys.exit(0)

if __name__ == '__main__':
    main()