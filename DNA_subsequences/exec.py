__author__ = 'rafael'
from commands import getoutput as run

cmd = "python3 dna_multi.py "
run("echo 1,2,3,4,6,8,12 > log")
for i in range(100):
    buffer = []
    for j in (1,2,3,4,6,8,12):
        print(i,j)
	buffer.append('\"'+run(cmd+str(j))[:-1].replace('.',',')+'\"')
    buffer = ','.join(buffer)
    run("echo "+buffer+" >> log")
