__author__ = 'rafael'
from time import time

def wtime():
    return time().real

def read_in():
    dic = {}
    with open('dna.in') as dna:
        ls = dna.readlines()
        nome = ls[0]
        dic[nome] = ""
        for l in ls[1:]:
            if l[0] != '>':
                dic[nome] += l[:-1]
            else:
                nome = l
                dic[nome] = ""
    return dic

def busca (query, dna):
    ret = []
    # organiza partes do genoma
    parts = list(dna.keys())
    parts.sort()
    parts = [parts[0]]+parts[2:]+[parts[1]]

    # parte a parte
    for nome in parts:
        # busca
        res = dna[nome].find(query)
        # se encontrou
        if res != -1:
            pass
            ret.append([nome,res])
    return ret

def main():
    dna = read_in()
    query = open('query.in')
    out = open('dna.out','w')

    start_time = wtime()
    while True:
        # Le nome da query de busca
        nome = query.readline()
        # testa se arquivo terminou
        if len(nome) == 0:
            break
        elif nome[0] == '>':
            # escreve nome da busca na saida
            out.write(nome)
            # le linha a ser buscada e remove \n
            search = query.readline()[:-1]
            # recebe resultado da busca
            res = busca(search,dna)
            # se busca retornou algum resultado
            if len(res) > 0:
                # itera resultados da busca
                for part,pos in res:
                    # escreve resultados
                    out.write(part)
                    out.write(str(pos)+"\n")
            else:
                out.write('NOT FOUND\n')
    end_time = wtime()

    query.close()
    out.close()
    print (end_time-start_time)

if __name__ == '__main__':
    main()