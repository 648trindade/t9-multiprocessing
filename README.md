# LEIA-ME #

## Usos: ##

### dotProd ###
* python3 **dotprod.py** <tam_mat> <num_rep>
* python3 **dotprod_multi_process.py** <tam_mat> <num_rep> <num_proc>
* python3 **dotprod_multi_pool.py** <tam_mat> <num_rep> <num_proc>

** onde: **

* **tam_mat**  - Tamanho das matrizes
* **num_rep**  - Número de repetições
* **num_proc** - Número de processos

### DNA_subsequences ###
* python3 **dna.py**
* python3 **dna_multi_process.py** <num_proc>
* python3 **dna_multi_pool.py** <num_proc>

** onde: **

* **num_proc** - Número de processos