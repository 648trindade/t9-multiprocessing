import sys, os
from multiprocessing import Pool

from time import time

def wtime():
    '''
    Mede tempo (wallclock) em segundos
    :return: float
    '''
    return time().real

def init_vectors(n):
    '''
    Inicializa vetores
    :param n: largura dos vetores
    :return: [[float],[float]]
    '''
    a = [0.5 for i in range(n)]
    b = [1.0 for i in range(n)]
    return [a,b]

def dot_product(bundle):
    '''
    Calcula o produto escalar (varias vezes)
    :param bundle: pacote de vetores e no de repeticoes
    :return: float
    '''
    a,b,repeat = bundle
    print('pid',os.getpid())
    for i in range(repeat):
        dot = 0.0
        for i in range(len(a)):
            dot += a[i] * b[i]
    return dot

def main():

    if len(sys.argv) is not 4:
        print("Uso:",sys.argv[0],"<tamanho dos vetores> <repeticoes> <processos>")
        sys.exit(1)

    n = int(sys.argv[1])
    repeat = int(sys.argv[2])
    k = int(sys.argv[3])

    a,b = init_vectors(n)
    bundle = [[a,b,repeat//k] for i in range(k)]
    for i in range(repeat%k):
        bundle[i][2] += 1

    start_time = wtime()
    with Pool(k) as p:
        dot = p.map(dot_product,bundle)
    end_time = wtime()

    print("Produto escalar =",dot[0]);
    print("Tempo de calculo =",end_time - start_time,"seg");

    sys.exit(0)

if __name__ == '__main__':
    main()