import sys

__author__ = 'rafael'

from time import time

def wtime():
    '''
    Mede tempo (wallclock) em segundos
    :return: float
    '''
    return time().real

def init_vectors(n):
    '''
    Inicializa vetores
    :param n: largura dos vetores
    :return: [[float],[float]]
    '''
    a = [0.5 for i in range(n)]
    b = [1.0 for i in range(n)]
    return [a,b]

def dot_product(a,b,n,repeat):
    '''
    Calcula o produto escalar (varias vezes)
    :param a: vetor a
    :param b: vetor b
    :param n: tamanho dos vetores
    :param repeat: taxa de repetição
    :return: float
    '''
    for i in range(repeat):
        dot = 0.0
        for i in range(n):
            dot += a[i] * b[i]
    return dot

def main():
    if len(sys.argv) is not 3:
        print("Uso:",sys.argv[0],"<tamanho dos vetores> <repeticoes>")
        sys.exit(1)

    n = int(sys.argv[1])
    repeat = int(sys.argv[2])

    a,b = init_vectors(n)

    start_time = wtime()
    dot = dot_product(a,b,n,repeat)
    end_time = wtime()

    print("Produto escalar =",dot);
    print("Tempo de calculo =",end_time - start_time,"seg");

    sys.exit(0)

if __name__ == '__main__':
    main()