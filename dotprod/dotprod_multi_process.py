import sys
from multiprocessing import Process, Queue

from time import time

def wtime():
    '''
    Mede tempo (wallclock) em segundos
    :return: float
    '''
    return time().real

def init_vectors(n):
    '''
    Inicializa vetores
    :param n: largura dos vetores
    :return: [[float],[float]]
    '''
    a = [0.5 for i in range(n)]
    b = [1.0 for i in range(n)]
    return [a,b]

def dot_product(a,b,n,repeat,q_dot):
    '''
    Calcula o produto escalar (varias vezes)
    :param a: vetor a
    :param b: vetor b
    :param n: tamanho dos vetores
    :param repeat: taxa de repetição
    '''
    for i in range(repeat):
        dot = 0.0
        for i in range(n):
            dot += a[i] * b[i]
    q_dot.put(dot)

def main():

    if len(sys.argv) is not 4:
        print("Uso:",sys.argv[0],"<tamanho dos vetores> <repeticoes> <processos>")
        sys.exit(1)

    n = int(sys.argv[1])
    repeat = int(sys.argv[2])
    k = int(sys.argv[3])
    dot = Queue()

    a,b = init_vectors(n)
    thr = [Process(target=dot_product,args=(a,b,n,repeat//k,dot)) for i in range(k)]

    start_time = wtime()
    for t in thr:
        t.start()
    for t in thr:
        t.join()
    end_time = wtime()

    print("Produto escalar =",dot.get());
    print("Tempo de calculo =",end_time - start_time,"seg");

    sys.exit(0)

if __name__ == '__main__':
    main()